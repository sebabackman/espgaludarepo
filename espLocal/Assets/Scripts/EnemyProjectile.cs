using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class EnemyProjectile : MonoBehaviour
{
    LevelManager lm;
    PlayerMovement pm;
    GameManager gm;
    Rigidbody2D rb;
    SpriteRenderer sr;

    public GameObject goldObject;
    public TextMesh multiplierText;
    
    public float slowSpeed;
    public Color slowColor;
    public float fastSpeed;
    public Color fastColor;

    Vector3 normalSpeed;
    public Color normalColor;

    public float spinning = 0f;

    bool failsafe = false;

    int currentLevel = 0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        gm.EnemyProjectileObjectRemove(this.gameObject, false);
        Destroy(this.gameObject);
    }

    void Awake()
    {
        lm = FindObjectOfType<LevelManager>();
        pm = FindObjectOfType<PlayerMovement>();
        gm = FindObjectOfType<GameManager>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        normalSpeed = rb.velocity;
        currentLevel = lm.levelSelected;
    }

    void Update()
    {
        rb.rotation += spinning * Time.deltaTime;
        if (/*Input.GetKeyDown(gm.resetKey)*/gm.resetSignal || lm.levelSelected != currentLevel)
        {
            Destroy(this.gameObject);
        }
        if (gm.kakuseiModeSwitch && gm.gemCounter > 0 && /*!Input.GetKeyDown(gm.resetKey)*/!gm.resetSignal && !failsafe && gm.EnemyProjectileGoldExplosion(this.gameObject))
        {
            failsafe = true;
            TextMesh customiseText = Instantiate(multiplierText, transform.position, pm.transform.rotation);
            Destroy(customiseText.gameObject, 0.4f);
            if (gm.totalBulletsCancelled >= 100)
            {
                customiseText.fontSize = 300;
            }
            customiseText.text = gm.totalBulletsCancelled.ToString();
            Instantiate(goldObject, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
        if (gm.kakuseiModeSwitch && gm.gemCounter != 0)
        {
            sr.color = slowColor;
            if (gm.kakuseiOverModeExtraTotalBulletSpeed != 0)
            {
                rb.velocity = normalSpeed / slowSpeed * gm.kakuseiOverModeExtraTotalBulletSpeed;
            }
            else
                rb.velocity = normalSpeed / slowSpeed;
        }
        else if (gm.kakuseiModeSwitch && gm.gemCounter == 0)
        {
            sr.color = fastColor;
            rb.velocity = normalSpeed * (fastSpeed + gm.kakuseiOverModeExtraTotalBulletSpeed);
        }
        else
        {
            sr.color = normalColor;
            if (gm.kakuseiOverModeExtraTotalBulletSpeed != 0)
            {
                rb.velocity = normalSpeed * gm.kakuseiOverModeExtraTotalBulletSpeed;
            }
            else
            {
                rb.velocity = normalSpeed;
            }
        }
    }
}
