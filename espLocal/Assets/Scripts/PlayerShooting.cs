using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomAmmo
{
    public string folderName;
    [Header("Basic")]
    public GameObject ammoObject;
    public Color ammoColor = Color.white;
    public bool lifetimeOnOff = true;
    [Range(0.0f, 10f)]
    public float projectileLifetime = 1f;
    [Range(0, 5)]
    public int amountOfLines = 1;
    public bool doubleAmmo = false;
    public float damage = 2;

    public bool playerMovementSlowOnOff = false;
    public float playerMovementSlowDown = 0.3f;

    [Header("Barrier Attack")]
    public bool isBarrierAttack = false;
    public GameObject barrierObject;
    public float barrierAttackMinWarmUpTime = 0.5f;
    public float barrierUpTime = 2;
    public float barrierGrowth = 0.5f;
    


    [Header("Primary Weapon short start burst")]
    public bool isStartBurst;
    public float startBurstTimer = 1f;

    [Header("Bullet")]
    public bool isBullet = true;
    public bool isStiff = false;
    [Range(0.1f, 4)]
    public float projectileWidth = 0.2f;
    [Range(0.1f, 4)]
    public float projectileLenght = 0.2f;
    [Range(0.0005f, 5)]
    public float fireRate = 0.1f;
    [Range(1, 128)]
    public float projectileSpeed = 1f;

    public bool burstFireOnOff = false;
    [Range(1, 32)]
    public int burstCount = 3;
    [Range(0.1f, 32)]
    public float burstCooldown = 0.5f;
    
    [Header("Laser")]
    public bool isLaser = false;
    [Range(0.1f, 32)]
    public float laserMaxDistance = 1f;
    [Range(0.1f, 32)]
    public float rotate = 0f;
    [Range(0.1f, 8)]
    public float laserWidht = 0.3f;
    [Range(0.0f, 8)]
    public float laserLenght = 0.3f;

    [Header("Extras")]
    [Range(0, 90)]
    public float startAngle = 0f;
    [Range(0f, 4)]
    public float spacingFromPlayer = 0.5f;
    [Range(0f, 4)]
    public float spacingFromOtherProjectiles = 0.2f;

    [Header("Widening Shooting")]
    public bool wideningShotOnOff = false;
    [Range(0.1f, 90)]
    public float shotAngle = 5f;
    [Range(0.001f, 0.5f)]
    public float wideningSpeed = 0.005f;
    [Range(0.001f, 0.5f)]
    public float recoverySpeed = 0.01f;

    [Header("Don't touch!")]
    public float leftDir = 0;
    public float rightDir = 0;
    public int lasersShot = 0;
    public float burstTimer = 0;
    public int burstsShot = 0;
    public float fireRateTimer = 0;
    public float startTime = 0;
}

[System.Serializable]
public class WeaponPowerLevel
{
    public string folderName;
    public List<CustomAmmo> weaponAmmo = new List<CustomAmmo>();
}

[System.Serializable]
public class WeaponType
{
    public string folderName;
    public List<WeaponPowerLevel> weaponPowerLevel = new List<WeaponPowerLevel>();
}

public class PlayerShooting : MonoBehaviour
{
    GameManager gm;
    PlayerMovement pm;
    public List<WeaponType> weaponType = new List<WeaponType>();
    
    [Header("Barrier Attack options")]
    public float barrierEnergyTankCapacityMeter = 2000f;
    public float barrierEnergyDrain = 1f;
    public float barrierAttackSize = 1f;
    public float barrierAttackFiringDuration = 1f;
    public float barrierAttackMinWarmUpTime = 1f;

    [Header("Don't touch!")]
    public float totalWidening = 0f;
    public float totalSpacing = 0f;

    public int type = 0;
    public int currentWeapon = 0;
    public int modules = 0;

    public int powerUpLevel = 0;
    //public int savePowerUpLevel = 0;
    public Quaternion target1;

    int previousType = 0;
    public bool barrierAttackInProgress = false;
    float barrierAttackWarmUpTimer = 0f;
    public float barrierEnergyTankOrginalCapacitySize = 0;
    int barrierAttackPowerLevel = 0;
    float barrierStartDelay = 0.15f;
    float barrierDelayTimer = 0;
    public float barrierAttackDurationTimer = 0;
    
    public GameObject barrierEnergyTankObject;
    public Vector3 barrierEnergyTankObjectOrginalScale;
    public float barrierEnergyTankScalingNumber;

    public float barrierAttackAddMoreDamageMultiplier = 1;

    GameObject barrierTarget;

    void Start()
    {
        //savePowerUpLevel = powerUpLevel;
        pm = FindObjectOfType<PlayerMovement>();
        gm = FindObjectOfType<GameManager>();
        barrierEnergyTankOrginalCapacitySize = barrierEnergyTankCapacityMeter;
        barrierEnergyTankObjectOrginalScale = barrierEnergyTankObject.transform.localScale;
    }
    
    public void WeaponPowerUp(bool isMaxPower)
    {
        if (isMaxPower)
        {
            powerUpLevel = 4;
        }
        else
        {
            if (powerUpLevel < 4)
            {
                powerUpLevel++;
            }
        }
        //savePowerUpLevel = powerUpLevel;
    }

    public void DestroyBullet(GameObject target)
    {
        if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].lifetimeOnOff)
        {
            Destroy(target.gameObject, weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].projectileLifetime);
        }
    }

    public void BarrierMode()
    {
        currentWeapon = type = 4;
        if (!barrierAttackInProgress)
        {
            barrierDelayTimer = barrierStartDelay;
            barrierTarget = Instantiate(weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[0].barrierObject, transform.position, transform.rotation);
            barrierAttackInProgress = true;
            barrierAttackWarmUpTimer = barrierAttackMinWarmUpTime;
        }
        if (barrierAttackInProgress)
        {
            if (/*Input.GetKey(gm.barrierKey)*/gm.barrierSignal && barrierEnergyTankCapacityMeter > 0 && barrierAttackDurationTimer <= 0 && barrierDelayTimer <= 0)
            {
                barrierAttackAddMoreDamageMultiplier += 10;
                barrierTarget.transform.localScale += new Vector3(weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[0].barrierGrowth,
                    weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[0].barrierGrowth) * Time.fixedDeltaTime;
                barrierEnergyTankCapacityMeter -= Time.fixedDeltaTime + barrierEnergyDrain;
            }
            if (/*!Input.GetKey(gm.barrierKey)*/!gm.barrierSignal && barrierAttackWarmUpTimer <= 0 || barrierEnergyTankCapacityMeter <= 0 && barrierAttackWarmUpTimer <= 0 && barrierDelayTimer <= 0)
            {
                barrierAttackAddMoreDamageMultiplier += 5;
                if (barrierEnergyTankCapacityMeter  < 0)
                {
                    barrierEnergyTankCapacityMeter = 0;
                }
                barrierAttackDurationTimer = barrierAttackFiringDuration;
                Destroy(barrierTarget, barrierAttackFiringDuration);
            }
        }
    }

    void ShootingManager()
    {
        //Normal primary 0, Kakusei primary 1, Normal secondary 2, Kakusei secondary 3, Barrier attack 4
        //Barrier Attack
        if (!barrierAttackInProgress && barrierAttackDurationTimer <= 0)
        {
            type = 100;
            if (/*Input.GetKey(gm.barrierKey)*/gm.barrierSignal && barrierEnergyTankCapacityMeter > 0)
            {
                barrierEnergyTankCapacityMeter -= barrierEnergyTankOrginalCapacitySize / 4;
                BarrierMode();
            }
            //Normal
            else if (/*Input.GetKey(gm.primaryWeaponKey)*/gm.primaryWeaponSignal && type == 100 && !gm.kakuseiModeSwitch && type != 4)
            {
                currentWeapon = type = 0;
                if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[0].isStartBurst)
                {
                    if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[0].startTime < 0)
                    {
                        currentWeapon = type = 0;
                    }
                    else
                    {
                        currentWeapon = type = 1;
                    }
                }
            }
            else if (/*Input.GetKey(gm.secondaryWeaponKey)*/gm.secondaryWeaponSignal && type == 100 && !gm.kakuseiModeSwitch && type != 4)
            {
                currentWeapon = type = 1;
            }
            //Kakusei
            else if (/*Input.GetKey(gm.primaryWeaponKey)*/gm.primaryWeaponSignal && type == 100 && gm.kakuseiModeSwitch && type != 4)
            {
                powerUpLevel = 0;
                currentWeapon = type = 2;
                if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[0].isStartBurst)
                {
                    if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[0].startTime < 0)
                    {
                        currentWeapon = type = 2;
                    }
                    else
                    {
                        currentWeapon = type = 3;
                    }
                }
            }
            else if (/*Input.GetKey(gm.secondaryWeaponKey)*/gm.secondaryWeaponSignal && type == 100 && gm.kakuseiModeSwitch && type != 4)
            {
                powerUpLevel = 0;
                currentWeapon = type = 3;
            }
            //Reset
            else if (/*!Input.GetKey(gm.primaryWeaponKey)*/!gm.primaryWeaponSignal && /*!Input.GetKey(gm.secondaryWeaponKey)*/!gm.secondaryWeaponSignal && /*!Input.GetKey(gm.barrierKey)*/!gm.barrierSignal)
            {
                type = 100;
                pm.slowDownMultiplier = 1;
            }
        }
        //Main shooting system
        if (/*Input.GetKey(gm.primaryWeaponKey)*/gm.primaryWeaponSignal && type != 100 && !barrierAttackInProgress || /*Input.GetKey(gm.secondaryWeaponKey)*/gm.secondaryWeaponSignal 
            && type != 100 && !barrierAttackInProgress && pm.stunTimer <= 0 || barrierAttackDurationTimer > 0)
        {
            while (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo.Count > modules)
            {
                if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].lasersShot < 1 && weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].isLaser 
                    || !weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].isLaser && weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstTimer <= 0 
                    && weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].fireRateTimer <= 0)
                {
                    //Spawn all the rows
                    for (int a = 0; a < weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].amountOfLines; a++)
                    {
                        //right
                        var bullet1 = Instantiate(weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].ammoObject, transform.position 
                            + new Vector3(totalSpacing = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].spacingFromPlayer + totalWidening, 0f), transform.rotation);
                        if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].isLaser)
                        {
                            bullet1.gameObject.GetComponentInChildren<SpriteRenderer>().color = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].ammoColor;
                        }
                        //start angle
                        if (!weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].isLaser)
                        {
                            bullet1.gameObject.GetComponent<SpriteRenderer>().color = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].ammoColor;
                            bullet1.gameObject.GetComponentInChildren<SpriteRenderer>().color = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].ammoColor;
                            bullet1.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, -weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].startAngle), 1f);
                            //dynamic widening
                            if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].wideningShotOnOff)
                            {
                                target1 = Quaternion.Euler(0, 0, -weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].shotAngle
                                    + -weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].startAngle);
                                pm.ShotMovingDirection();
                                bullet1.transform.rotation = Quaternion.Slerp(bullet1.transform.rotation, target1, pm.rightDir);
                            }
                        }
                        //left
                        if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].doubleAmmo)
                        {
                            target1 = Quaternion.Euler(0, 0, weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].shotAngle 
                                + weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].startAngle);
                            var bullet2 = Instantiate(weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].ammoObject, transform.position 
                                + new Vector3(totalSpacing = -1f * (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].spacingFromPlayer + totalWidening), 0f), transform.rotation);
                            if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].isLaser)
                            {
                                bullet2.gameObject.GetComponentInChildren<SpriteRenderer>().color = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].ammoColor;
                            }
                            //start angle
                            if (!weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].isLaser)
                            {
                                bullet2.gameObject.GetComponent<SpriteRenderer>().color = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].ammoColor;
                                bullet2.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].startAngle), 1f);
                                //dynamic widening
                                if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].wideningShotOnOff)
                                {
                                    pm.ShotMovingDirection();
                                    bullet2.transform.rotation = Quaternion.Slerp(bullet2.transform.rotation, target1, pm.leftDir);
                                }
                            }
                        }
                        totalWidening += weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].spacingFromOtherProjectiles;
                    }
                    //Burst fire cooldown
                    weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstsShot++;
                    if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstsShot >= weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstCount 
                        && weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstFireOnOff)
                    {
                        weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstsShot = 0;
                        weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstTimer = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].burstCooldown;
                    }
                    totalWidening = 0;
                }
                //Shoot only one row of lasers
                if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].isLaser)
                {
                    weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].lasersShot++;
                }
                if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].fireRateTimer <= 0)
                {
                    weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].fireRateTimer = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].fireRate;
                }
                //Player movement slowing
                if (weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].playerMovementSlowOnOff)
                {
                    pm.slowDownMultiplier = weaponType[type].weaponPowerLevel[powerUpLevel].weaponAmmo[modules].playerMovementSlowDown;
                }
                else
                {
                    pm.slowDownMultiplier = 1;
                }
                modules++;
            }
            modules = 0;
            totalWidening = 0f;
        }
        
        //powerUpLevel = savePowerUpLevel;
    }

    void FixedUpdate()
    {
        if (gm.levelOnOff)
        {
            barrierEnergyTankScalingNumber = barrierEnergyTankCapacityMeter / barrierEnergyTankOrginalCapacitySize;
            barrierEnergyTankObject.transform.localScale = new Vector3(barrierEnergyTankObjectOrginalScale.x * barrierEnergyTankScalingNumber, barrierEnergyTankObject.transform.localScale.y);

            //Delete laser objects
            if (type != previousType || pm.stunTimer > 0)
            {
                previousType = type;
                for (int a = 0; a < weaponType.Count; a++)
                {
                    for (int b = 0; b < weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo.Count; b++)
                    {
                        weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].lasersShot = 0;
                    }
                }
            }
            //Count down weapon timers
            for (int a = 0; a < weaponType.Count; a++)
            {
                for (int b = 0; b < weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo.Count; b++)
                {
                    weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].fireRateTimer -= Time.fixedDeltaTime;
                    weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].startTime -= Time.fixedDeltaTime;
                    if (weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].isStartBurst && /*!Input.GetKey(gm.primaryWeaponKey)*/!gm.primaryWeaponSignal 
                        && /*!Input.GetKey(gm.secondaryWeaponKey)*/!gm.secondaryWeaponSignal && /*!Input.GetKey(gm.barrierKey)*/!gm.barrierSignal)
                    {
                        weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].startTime = weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].startBurstTimer;
                    }
                }
            }
            //reset not in use weapon angles
            for (int a = 0; a < weaponType.Count; a++)
            {
                if (a != currentWeapon || a == currentWeapon && /*!Input.GetKey(gm.secondaryWeaponKey)*/!gm.secondaryWeaponSignal && /*!Input.GetKey(gm.primaryWeaponKey)*/!gm.primaryWeaponSignal)
                {
                    for (int b = 0; b < weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo.Count; b++)
                    {
                        weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].leftDir = 0;
                        weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].rightDir = 0;
                    }
                }
            }
            for (int a = 0; a < weaponType.Count; a++)
            {
                for (int b = 0; b < weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo.Count; b++)
                {
                    if (weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].burstFireOnOff)
                    {
                        weaponType[a].weaponPowerLevel[powerUpLevel].weaponAmmo[b].burstTimer -= Time.deltaTime;
                    }
                }
            }
            if (barrierAttackDurationTimer > 0)
            {
                barrierAttackDurationTimer -= Time.fixedDeltaTime;
                if (barrierAttackDurationTimer <= 0)
                {
                    barrierAttackInProgress = false;
                }
            }
            if (barrierAttackWarmUpTimer > 0)
            {
                barrierAttackWarmUpTimer -= Time.fixedDeltaTime;
            }
            if (pm.stunTimer <= 0)
            {
                ShootingManager();
            }
            if (barrierDelayTimer > 0)
            {
                barrierDelayTimer -= Time.fixedDeltaTime;
            }
            if (barrierAttackInProgress && barrierAttackDurationTimer <= 0)
            {
                BarrierMode();
            }
            if (barrierAttackDurationTimer <= 0)
            {
                barrierAttackAddMoreDamageMultiplier = 0;
            }
        }
    }
}