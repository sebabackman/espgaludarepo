using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    ScoreManager sm;
    public bool isMaxPower = false;
    PlayerShooting ps;
    public float speed = 5;
    public Vector3 liike;
    GameObject wallLeft;
    GameObject wallRight;
    GameObject wallTop;
    GameObject wallBottom;
    private void Awake() 
    {
        sm = FindObjectOfType<ScoreManager>();
        ps = FindObjectOfType<PlayerShooting>();
        liike = new Vector2(Random.Range(-15, 15), Random.Range(-15, 15));
        wallLeft = GameObject.Find("WallLeft");
        wallRight = GameObject.Find("WallRight");
        wallTop = GameObject.Find("WallTop");
        wallBottom = GameObject.Find("WallBottom");
    }
    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.name == "PlayerContainer") {
            ps.WeaponPowerUp(isMaxPower);
            Destroy(this.gameObject);
        }
    }
    private void Update() 
    {
        if (sm.currentScore <= 0)
        {
            Destroy(this.gameObject);
        }

        this.transform.position += liike.normalized * Time.deltaTime * speed;
        if (this.transform.position.y >= wallTop.transform.position.y) {
            liike = liike * new Vector2(1, -1);
        }
        if (this.transform.position.y <= wallBottom.transform.position.y) {
            liike = liike * new Vector2(1, -1);
        }
        if (this.transform.position.x >= wallRight.transform.position.x) {
            liike = liike * new Vector2(-1, 1);
        }
        if (this.transform.position.x <= wallLeft.transform.position.x) {
            liike = liike * new Vector2(-1, 1);
        }

    }
}