using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour
{
    ScoreManager sm;
    PlayerMovement pm;
    GameManager gm;
    public int pointsFromGold = 100;
    public float speed = 20f;

    void Awake()
    {
        sm = FindObjectOfType<ScoreManager>();
        pm = FindObjectOfType<PlayerMovement>();
        sm = FindObjectOfType<ScoreManager>();
        gm = FindObjectOfType<GameManager>();

        if (gm.goldCounter >= 900) {
            pointsFromGold = 900;
        }
        else if (gm.goldCounter >= 800) {
            pointsFromGold = 700;
        }
        else if (gm.goldCounter >= 700) {
            pointsFromGold = 650;
        }
        else if (gm.goldCounter >= 600) {
            pointsFromGold = 600;
        }
        else if (gm.goldCounter >= 500) {
            pointsFromGold = 400;
        }
        else if (gm.goldCounter >= 400) {
            pointsFromGold = 350;
        }
        else if (gm.goldCounter >= 300) {
            pointsFromGold = 300;
        }
        else if (gm.goldCounter >= 200) {
            pointsFromGold = 200;
        }
        else if (gm.goldCounter >= 100) {
            pointsFromGold = 150;
        }
        else {
            pointsFromGold = 100;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "PlayerContainer")
        {
            gm.AddGold();
            sm.AddScore(pointsFromGold);
            Destroy(this.gameObject);
        }
    }

    void Update()
    {
        if (sm.currentScore <= 0)
        {
            Destroy(this.gameObject);
        }
        Vector3 playerpos = pm.transform.position;
        Vector3 delta = playerpos - this.transform.position;
        this.transform.position += delta.normalized * speed * Time.deltaTime;
    }
}
