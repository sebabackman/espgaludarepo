using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    GameManager gm;
    PlayerShooting ps;

    public int bulletCancelPoints = 40;
    public int constantScore = 10;

    [Header("Don't touch!")]
    public int currentScore = 0;
    public int preScore = 0;
    float timer = 0;

    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        ps = FindObjectOfType<PlayerShooting>();
    }

    public void BulletCancelPoints(int totalCancelled)
    {
        if (totalCancelled < 100)
        {
            preScore += bulletCancelPoints * totalCancelled / 2;
        }
        else
        {
            preScore += bulletCancelPoints * totalCancelled;
        }
    }

    public void AddScore(int score) {
        preScore += score;
    }

    void ScoreDepositor()
    {
        if (currentScore < preScore)
        {
            if (preScore - currentScore > 100000)
            {
                currentScore += 100000;
            }
            else if (preScore - currentScore > 10000)
            {
                currentScore += 10000;
            }
            else if (preScore - currentScore > 1000)
            {
                currentScore += 1000;
            }
            else
            {
                currentScore += 10;
            }
            gm.playerScore.text = currentScore.ToString();
        }
    }

    void Update()
    {
        ScoreDepositor();
        if (timer < 0)
        {
            timer = 0.25f;
        }
        timer -= Time.deltaTime;
    }
}
