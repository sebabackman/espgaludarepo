using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

[System.Serializable]
public class EnemyProjectileManager
{
    public int enemyObject = 0;
    public bool notPlayer = true;
    public List<int> enemyProjectiles = new List<int>();
}

[System.Serializable]
public class KakuseiClockBallManager
{
    public int clockBallId = 0;
    public Transform clockBallTransform;
}

public class GameManager : MonoBehaviour
{
    PlayerShooting ps;
    ScoreManager sm;
    PlayerMovement pm;
    GameManager gm;
    MenuManager mm;
    [Header("Don't touch!")]
    public List<EnemyProjectileManager> enemyProjectileManager = new List<EnemyProjectileManager>();
    public bool kakuseiModeSwitch = false;
    [Space]
    
    public Text playerScore;
    public List<GameObject> lifeBar = new List<GameObject>();
    public Text gemCountText;
    public Text goldCountText;
    public Text continueText;
    
    public bool levelOnOff = true;

    [Header("Life stuff")]
    public int maxLifes = 3;
    public List<GameObject> lifeObjects = new List<GameObject>();
    public int lifeCounter = 0;

    public int gemCounter = 0;
    public int goldCounter = 0;

    float gemTimer = 0;
    float overmodePointsTimer = 0;

    [Header("Item prefabs")]
    public GameObject lifeObject;
    public GameObject goldObject;
    public GameObject powerUpObject;
    public GameObject maxPowerObject;
    public GameObject energyObject;

    

    [Header("Controller Mapping")]
    PlayerControlls controll;
    //public PlayerControlls primaryWeaponController;
    //public InputControl secondaryWeaponController;
    //public InputControl kakuseiController;
    //public InputControl barrierController;
    //public InputControl resetController;
    //public InputControl wallpaperController;

    [Header("KeyBoard Mapping")]
    public KeyCode primaryWeaponKeyBoard;
    public KeyCode secondaryWeaponKeyBoard;
    public KeyCode kakuseiKeyBoard;
    public KeyCode barrierKeyBoard;
    public KeyCode resetKeyBoard;
    public KeyCode wallpaperKeyBoard;

    [Space]
    public List<Sprite> wallpapers = new List<Sprite>();
    GameObject wallpaperHolder;
    int currentWallpaper = 0;

    [Header("Kakusei analog clock of balls")]
    public GameObject KakuseiClockBallPrefab;
    public int maxClockBalls = 8;
    public float distanceFromPlayer = 1f;
    public List<KakuseiClockBallManager> listOfClockBalls = new List<KakuseiClockBallManager>();

    public Color kakuseiModeColor = Color.white;
    public Color kakuseiOverModeColor = Color.blue;

    float kakuseiModeClockVisibleValue = 0;

    public float kakuseiOverModeTimeUntilFull = 4f;
    public float kakuseiOverModeExtraBulletSpeedStep = 0.5f;
    public int kakuseiOverModeExtraPassivePointStep = 10;
    public int kakuseiOvermodeExtraGemDropStep = 2;

    [Header("Kakusei tick points")]
    public float kakuseiOvermodeTickTime = 1f;

    [Header("Don't touch!")]
    public int kakuseiOverModeExtraTotalGemDrop = 0;
    public int kakuseiOverModeExtraTotalPassivePoints = 0;
    public float kakuseiOverModeExtraTotalBulletSpeed = 0;
    public float kakuseiOverModeTimer = 0;
    float kakuseiOverModeClockVisibleValue = 0;

    [Space]
    public bool primaryWeaponSignal = false;
    public bool secondaryWeaponSignal = false;
    public bool kakuseiSignal = false;
    public bool barrierSignal = false;
    public bool resetSignal = false;
    public bool wallpaperSignal = false;

    [Space]
    public GameObject flashObject;
    public float continueCountDown = 10;
    public bool continueInput = false;
    public float continueTimer = 10;
    
    float realTime = 0;
    [Space]
    public int totalBulletsCancelled = 0;

    private void Awake()
    {
        controll = new PlayerControlls();
    }

    private void OnEnable()
    {
        controll.Gameplay.Enable();
    }

    void Start()
    {
        realTime = Time.realtimeSinceStartup;
        gm = FindObjectOfType<GameManager>();
        ps = FindObjectOfType<PlayerShooting>();
        pm = FindObjectOfType<PlayerMovement>();
        sm = FindObjectOfType<ScoreManager>();
        mm = FindObjectOfType<MenuManager>();
        gemCountText.text = gemCounter.ToString();
        goldCountText.text = goldCounter.ToString();
        playerScore.text = "0";
        wallpaperHolder = GameObject.Find("Wallpaper");
        continueText.text = "";
    }
    
    public void InputMerger()
    {
        if (Input.GetKey(primaryWeaponKeyBoard) || controll.Gameplay.FocusFire.IsPressed())
        {
            primaryWeaponSignal = true;
        }
        else
        {
            primaryWeaponSignal = false;
        }
        if (Input.GetKey(secondaryWeaponKeyBoard) || controll.Gameplay.WideFire.IsPressed())
        {
            secondaryWeaponSignal = true;
        }
        else
        {
            secondaryWeaponSignal = false;
        }
        if (Input.GetKeyDown(kakuseiKeyBoard) || controll.Gameplay.KakuseiMode.WasPressedThisFrame())
        {
            kakuseiSignal = true;
        }
        else
        {
            kakuseiSignal = false;
        }
        if (Input.GetKey(barrierKeyBoard) || controll.Gameplay.BarrierAttack.IsPressed())
        {
            barrierSignal = true;
        }
        else
        {
            barrierSignal = false;
        }
        if (Input.GetKeyDown(resetKeyBoard) || controll.Gameplay.MapSwitch.WasPressedThisFrame())
        {
            resetSignal = true;
        }
        else
        {
            resetSignal = false;
        }
        if (Input.GetKeyDown(wallpaperKeyBoard) || controll.Gameplay.WallpaperSwitch.WasPressedThisFrame())
        {
            wallpaperSignal = true;
        }
        else
        {
            wallpaperSignal = false;
        }
    }

    public void OnLevelStart()
    {
        lifeCounter = maxLifes;
        for (int a = 0; a < lifeObjects.Count && a < lifeCounter; a++)
        {
            lifeObjects[a].SetActive(true);
            lifeObjects[a].transform.GetChild(0).gameObject.SetActive(true);
        }
        for (int a = maxLifes; a < lifeObjects.Count; a++)
        {
            if (lifeObjects[a].activeSelf)
            {
                lifeObjects[a].SetActive(false);
            }
        }
    }

    public void AddLife() 
    {
        if (lifeCounter < maxLifes)
        {
            lifeObjects[lifeCounter].transform.GetChild(0).gameObject.SetActive(true);
            lifeCounter++;
        }
    }

    public void RemoveLife()
    {
        if (lifeCounter > 0)
        {
            lifeCounter--;
            lifeObjects[lifeCounter].transform.GetChild(0).gameObject.SetActive(false);
            if (lifeCounter <= 0)
            {
                print("dead");
                continueTimer = continueCountDown;
                continueInput = false;
            }
        }
    }

    void ContinuePlaying()
    {
        Time.timeScale = 0;
        continueText.text = "Continue?\n(space)\n" + ((int)continueTimer);
        if(Input.GetKey(KeyCode.Space) && continueTimer > 0)
        {
            sm.currentScore = sm.preScore = 0;
            gemCounter = 0;
            goldCounter = 0;
            gm.playerScore.text = sm.currentScore.ToString();
            gemCountText.text = gemCounter.ToString();
            goldCountText.text = goldCounter.ToString();
            Time.timeScale = 1;
            continueInput = true;
            continueText.text = "";
            OnLevelStart();
        }
        else if (continueTimer <= 0)
        {
            continueText.text = "Game Over \n Press any key to continue.";
        }
    }

    public void AddGem()
    {
        if (gemCounter < 500)
        {
            gemCounter += 2;
        }
        if (gemCounter > 500)
        {
            gemCounter = 500;
        }
        gemCountText.text = gemCounter.ToString();
    }

    public void AddGold()
    {
        if (goldCounter < 1000)
        {
            goldCounter++;
        }
        goldCountText.text = goldCounter.ToString();
    }

    public void RemoveGold(bool bossFight)
    {
        if (bossFight)
        {
            goldCounter--;
        }
        else
        {
            goldCounter -= goldCounter / 2;
        }
        goldCountText.text = goldCounter.ToString();
    }

    public void EnemyProjectileRecords(GameObject enemyObj, GameObject enemyProjectile)
    {
        int a = 0;
        while (enemyProjectileManager.Count > a && enemyProjectileManager[a].enemyObject != 0 && enemyProjectileManager[a].enemyObject != enemyObj.GetInstanceID())
        {
            a++;
        }
        if (enemyProjectileManager.Count == 0 || enemyProjectileManager.Count <= a)
        {
            enemyProjectileManager.Add(new EnemyProjectileManager());
            enemyProjectileManager[a].enemyObject = enemyObj.GetInstanceID();
            enemyProjectileManager[a].enemyProjectiles.Add(enemyProjectile.GetInstanceID());
        }
        else
        {
            enemyProjectileManager[a].enemyObject = enemyObj.GetInstanceID();
            enemyProjectileManager[a].enemyProjectiles.Add(enemyProjectile.GetInstanceID());
        }
    }

    public void EnemyProjectileObjectRemove(GameObject obj, bool notPlayer)
    {
        int a = 0;
        while (enemyProjectileManager.Count > a)
        {
            if (enemyProjectileManager[a].enemyObject == obj.GetInstanceID())
            {
                if (!notPlayer)
                {
                    enemyProjectileManager[a].notPlayer = false;
                    enemyProjectileManager.RemoveAt(a);
                }
            }

            int b = 0;
            while (enemyProjectileManager.Count > a && enemyProjectileManager[a].enemyProjectiles.Count > b)
            {
                if (enemyProjectileManager[a].enemyProjectiles[b] == obj.GetInstanceID())
                {
                    if (enemyProjectileManager[a].enemyProjectiles.Count <= 1)
                    {
                        enemyProjectileManager.RemoveAt(a);
                    }
                    else
                    {
                        enemyProjectileManager[a].enemyProjectiles.RemoveAt(b);
                    }
                    return;
                }
                b++;
            }
            a++;
        }
    }

    public void ClearEnemyBulletLibrary()
    {
        enemyProjectileManager.Clear();
    }

    public bool EnemyProjectileGoldExplosion(GameObject obj)
    {
        int a = 0;
        while (enemyProjectileManager.Count > a)
        {
            int b = 0;
            while (enemyProjectileManager[a].enemyProjectiles.Count > b)
            {
                if (enemyProjectileManager[a].enemyProjectiles[b] == obj.GetInstanceID())
                {
                    return (false);
                }
                b++;
            }
            a++;
        }
        if (totalBulletsCancelled < 100)
        {
            totalBulletsCancelled += 1;
        }
        sm.BulletCancelPoints(totalBulletsCancelled);
        return (true);
    }

    void KakuseiManager()
    {
        int a = 0;
        while(listOfClockBalls.Count > a)
        {
            if (!kakuseiModeSwitch)
            {
                totalBulletsCancelled = 0;
                listOfClockBalls[a].clockBallTransform.GetComponent<SpriteRenderer>().enabled = false;
            }
            a++;
        }
        
        if (/*Input.GetKeyDown(kakuseiKey)*/kakuseiSignal)
        {
            kakuseiModeSwitch = !kakuseiModeSwitch;
            var quik = Instantiate(flashObject, pm.transform.position, Quaternion.Euler(0f, 0f, 45f));
            Destroy(quik, 0.1f);
        }
        if (kakuseiModeSwitch)
        {
            gemTimer -= Time.deltaTime;
            if (gemTimer <= 0)
            {
                if (gemCounter > 0)
                {
                    gemCounter--;
                }
                gemCountText.text = gemCounter.ToString();
                gemTimer = 0.25f;
            }

            //KakuseiClockBallSpawner
            if (listOfClockBalls.Count < maxClockBalls - 1)
            {
                for (int b = 0; b < maxClockBalls; b++)
                {
                    var radians = 2 * Mathf.PI / maxClockBalls * b;
                    var vertical = Mathf.Sin(radians + 0.7853982f * 2);
                    var horizontal = Mathf.Cos(radians + 0.7853982f * 2);
                    var spawnDir = new Vector3(horizontal, vertical, 0);
                    var spawnPos = pm.transform.position + spawnDir * distanceFromPlayer;
                    var ball = Instantiate(KakuseiClockBallPrefab, spawnPos, pm.transform.rotation);
                    ball.transform.parent = GameObject.Find("KakuseiClockMaster").transform;
                    listOfClockBalls.Add(new KakuseiClockBallManager());
                    listOfClockBalls[b].clockBallId = ball.gameObject.GetInstanceID();
                    listOfClockBalls[b].clockBallTransform = ball.transform;
                }
            }

            //Normalmode
            if (gemCounter > 0)
            {
                kakuseiModeClockVisibleValue = 500 / listOfClockBalls.Count;
                float kakuseiModeVisibleCurrentValue = 0;
                for (int c = 0; c < listOfClockBalls.Count; c++)
                {
                    if (kakuseiModeVisibleCurrentValue < gemCounter)
                    {
                        listOfClockBalls[c].clockBallTransform.GetComponent<SpriteRenderer>().enabled = true;
                        listOfClockBalls[c].clockBallTransform.GetComponent<SpriteRenderer>().color = kakuseiModeColor;
                    }
                    else
                    {
                        listOfClockBalls[c].clockBallTransform.GetComponent<SpriteRenderer>().enabled = false;
                    }
                    kakuseiModeVisibleCurrentValue += kakuseiModeClockVisibleValue;
                }
            }
            //Overmode
            else
            {
                if (kakuseiOverModeTimer < kakuseiOverModeTimeUntilFull)
                {
                    if (ps.type == 100)
                    {
                        kakuseiOverModeTimer += Time.deltaTime * 2;
                    }
                    else
                        kakuseiOverModeTimer += Time.deltaTime;
                }
                kakuseiOverModeExtraTotalBulletSpeed = 0;
                kakuseiOverModeExtraTotalPassivePoints = 0;
                kakuseiOverModeExtraTotalGemDrop = 0;
                kakuseiOverModeClockVisibleValue = kakuseiOverModeTimeUntilFull / listOfClockBalls.Count;
                int d = 1;
                while (kakuseiOverModeClockVisibleValue * d <= kakuseiOverModeTimer)
                {
                    kakuseiOverModeExtraTotalBulletSpeed += kakuseiOverModeExtraBulletSpeedStep;
                    kakuseiOverModeExtraTotalPassivePoints += kakuseiOverModeExtraPassivePointStep;
                    kakuseiOverModeExtraTotalGemDrop += kakuseiOvermodeExtraGemDropStep;
                    d++;
                }
                if (overmodePointsTimer < 0 && ps.type == 100)
                {
                    sm.preScore += kakuseiOverModeExtraTotalPassivePoints;
                    overmodePointsTimer = kakuseiOvermodeTickTime;
                }
                float kakuseiOverModeVisibleCurrentValue = 0;
                for (int c = 0; c < listOfClockBalls.Count; c++)
                {
                    if (kakuseiOverModeVisibleCurrentValue < kakuseiOverModeTimer)
                    {
                        listOfClockBalls[c].clockBallTransform.GetComponent<SpriteRenderer>().enabled = true;
                        listOfClockBalls[c].clockBallTransform.GetComponent<SpriteRenderer>().color = kakuseiOverModeColor;
                    }
                    else
                    {
                        listOfClockBalls[c].clockBallTransform.GetComponent<SpriteRenderer>().enabled = false;
                    }
                    kakuseiOverModeVisibleCurrentValue += kakuseiOverModeClockVisibleValue;
                }
            }
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        InputMerger();
        if (levelOnOff)
        {
            KakuseiManager();
            if (Input.anyKey && continueTimer <= 0 && continueInput == false)
            {
                mm.BackToMainMenu();
            }
        }
        overmodePointsTimer -= Time.deltaTime;
        if (!continueInput && lifeCounter <= 0 && continueTimer > 0)
        {
            continueTimer -= Time.unscaledTime - realTime;
            ContinuePlaying();
        }
        if (/*Input.GetKeyDown(wallpaperKey)*/wallpaperSignal)
        {
            currentWallpaper++;
            if (wallpapers.Count <= currentWallpaper)
            {
                currentWallpaper = 0;
            }
            wallpaperHolder.GetComponent<SpriteRenderer>().sprite = wallpapers[currentWallpaper];
        }
        realTime = Time.unscaledTime;
    }
}
