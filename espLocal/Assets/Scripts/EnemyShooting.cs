using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Type
{
    [Header("Basic")]
    public GameObject enemyBullet;
    public float firerate = 1f;
    public float projectileSpeed = 1f;
    public int BurstAmount = 1;

    [Header("")]
    public bool standardFire = false;
    [Space]
    public bool shotgun = false;
    public int shotgunPellets = 1;
    public float shotgunSpread = 1f;
    [Space]
    public bool waveRifle = false;

    [Space]
    public bool lineShot = false;
    
    [Space]
    public bool wallShot = false;

    [Space]
    public bool spiralShot = false;

    [Space]
    public bool circleShot = false;

    [Header("Don't touch!")]
    public float weaponTimer = 0f;
    public float firerateTimer = 0f;
    public int shotsShot = 0;
    
}

[System.Serializable]
public class Weapon
{
    public bool isGroundTurret = false;
    public List<Type> type = new List<Type>();
}

public class EnemyShooting : MonoBehaviour
{
    public List<Weapon> weapon = new List<Weapon>();
    bool andAction = false;

    BackgroundManager bm;
    PlayerMovement pm;
    GameManager gm;

    public int kakuseiBulletAmountMultiplier = 2;
    int currentWeapon = 0;
    int currentType = 0;
    public float cooldown = 1f;
    float timer = 0f;

    bool done = true;
    Vector3 torwardsPlayer = new Vector3(0f, 0f, 0f);
    Vector3 difference;

    private void Start()
    {
        pm = FindObjectOfType<PlayerMovement>();
        bm = FindObjectOfType<BackgroundManager>();
        gm = FindObjectOfType<GameManager>();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //take damage
        if (collision.gameObject != null)
        {
            if (collision.gameObject.name == "ActiveArea")
            {
                andAction = true;
            }
        }
    }

    void EnemyMainGun()
    {
        currentWeapon = 0;
        while (weapon.Count > currentWeapon && !done)
        {
            currentType = 0;
            while (weapon[currentWeapon].type.Count > currentType && !done)
            {
                if (weapon[currentWeapon].type[currentType].shotsShot >= weapon[currentWeapon].type[currentType].BurstAmount && !gm.kakuseiModeSwitch
                        || weapon[currentWeapon].type[currentType].shotsShot >= weapon[currentWeapon].type[currentType].BurstAmount * kakuseiBulletAmountMultiplier && gm.kakuseiModeSwitch)
                {
                    done = true;
                    timer = cooldown;
                    weapon[currentWeapon].type[currentType].shotsShot = 0;
                }
                else if (weapon[currentWeapon].type[currentType].firerateTimer <= 0 && !done)
                {
                    weapon[currentWeapon].type[currentType].firerateTimer = weapon[currentWeapon].type[currentType].firerate;
                    GameObject target = Instantiate(weapon[currentWeapon].type[currentType].enemyBullet, transform.position, transform.rotation);
                    float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
                    target.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ + 90);
                    target.GetComponent<Rigidbody2D>().velocity = (torwardsPlayer).normalized * weapon[currentWeapon].type[currentType].projectileSpeed * Time.fixedDeltaTime;
                    gm.EnemyProjectileRecords(this.gameObject, target);
                    weapon[currentWeapon].type[currentType].shotsShot++;
                }
                currentType++;
            }
            currentWeapon++;
        }
    }

    void FixedUpdate()
    {
        //Timers
        if (pm.immunityTimer <= 0)
        {
            for (int a = 0; a < weapon.Count; a++)
            {
                for (int b = 0; b < weapon[a].type.Count; b++)
                {
                    weapon[a].type[b].firerateTimer -= Time.deltaTime;
                }
            }
        }
        
        if (andAction)
        {
            timer -= Time.deltaTime;
            if (timer <= 0 && done)
            {
                done = false;
                difference = transform.position - pm.transform.position;
                torwardsPlayer = pm.transform.position - this.transform.position;
            }
            if (!done)
            {
                EnemyMainGun();
            }
        }
    }
}
