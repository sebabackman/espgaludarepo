using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierMover : MonoBehaviour
{
    PlayerMovement pm;
    GameManager gm;
    int ballSlot = 0;

    public bool isClockBall = false;
    void Start()
    {
        pm = FindObjectOfType<PlayerMovement>();
        gm = FindObjectOfType<GameManager>();

        if (isClockBall)
        {
            while (gm.listOfClockBalls.Count > ballSlot && gm.listOfClockBalls[ballSlot].clockBallId != gameObject.GetInstanceID())
            {
                ballSlot++;
            }
        }
    }


    void Update()
    {
        if (isClockBall)
        {
            

        }
        else
        {
            transform.position = pm.transform.position;
        }
    }
}
