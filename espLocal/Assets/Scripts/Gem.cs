using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour
{
    GameObject player;
    GameManager gm;
    ScoreManager sm;
    public float speed = 20;
    private void Awake() {
        player = GameObject.Find("PlayerContainer");
        gm = FindObjectOfType<GameManager>();
        sm = FindObjectOfType<ScoreManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.name == "PlayerContainer")
        {
            gm.AddGem();
            sm.AddScore(10);
            Destroy(this.gameObject);
        }
    }
    private void Update() {
        if (sm.currentScore <= 0)
        {
            Destroy(this.gameObject);
        }
        Vector3 playerpos = player.transform.position;
        Vector3 delta = playerpos - this.transform.position;
        this.transform.position += delta.normalized * speed * Time.deltaTime;
    }
}
