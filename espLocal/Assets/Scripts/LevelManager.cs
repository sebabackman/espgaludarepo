using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
//Sprite(map), EnemyLayoutPrefab, BossPrefab, music
public class Level
{
    public string floderName;
    [Header("Sprites")]
    public Sprite mapSprite;
    public Sprite bossMap1;
    public Sprite bossMap2;
    [Header("Scaling")]
    public float backgroundScaling = 1;
    public float bossFightBackgroundScaling = 1;
    public float minimapScaling = 1;
    public Vector2 minimapHiderScale = new Vector2(1f, 1f);
    public Vector2 minimapTransparentScale = new Vector2(1f, 1f);
    [Header("Sprite Positions")]
    public float backgroundSpeed = 1;
    public float minimapTransparentSpeed = 1;
    public float minimapTransparentYStartPos = 1;
    public float minimapHiderPos = 1;
    public float backgroundStartPos = 45f;
    public float kakuseiModeSlow = 2f;
    [Tooltip("Where the first bossfight background should start(used when level is reset).")]
    public float bossFightStartPos = 109.15f;
    [Tooltip("Where is the next bossfight background located from the previous one(used when level is reset).")]
    public float nextBossFightPos = 20f;

    [Header("Other")]
    public GameObject enemyLayoutPrefab;
    public List<EnemyGroups> enemyGroups = new List<EnemyGroups>();
    public AudioClip travelMusic;
    public AudioClip bossMusic;
}

//Enemy data
[System.Serializable]
public class EnemySpawnSystems
{
    public string folderName;
    [Header("Basic")]
    public GameObject enemyObject;
    //public int health = 100;
    public bool dropExtraLife = false;
    public bool dropPowerup = false;
    public bool dropMaxPowerup = false;
    public bool dropEnergy = false;

    [Header("Waypoint movement")]
    public bool isWaypoint = false;
    public List<Transform> destinations = new List<Transform>();
    public float speed = 20;

    [Header("Direct movement")]
    public bool isDirect = false;
    public float dXSpeed = 0f;
    public float dYSpeed = 0f;

    [Header("Wavy movement")]
    public bool isWavy = false;
    public bool horizontal = false;
    public float wavySpeed = 1f;
    public float waveSize = 1f;
    public float waveFrequency = 1f;
    public float waveDirection = 45f;

    //Tiket number to identify right objects
    [Header("Don't touch!")]
    public int currentEnemyObject = 999999;
}

//spawner data
[System.Serializable]
public class EnemyGroups
{
    public string folderName;
    public GameObject spawnerObject;
    public float spawnDelay = 1f;
    public List<EnemySpawnSystems> enemySpawnSystems = new List<EnemySpawnSystems>();
}

public class LevelManager : MonoBehaviour
{
    public List<Level> maps = new List<Level>();
    public int levelSelected = 0;

    List<Transform> enemyTransforms = new List<Transform>();

    BackgroundManager bm;
    PlayerShooting ps;
    VisualManager vm;
    GameManager gm;

    private void Start()
    {
        bm = FindObjectOfType<BackgroundManager>();
        ps = FindObjectOfType<PlayerShooting>();
        vm = FindObjectOfType<VisualManager>();
        gm = FindObjectOfType<GameManager>();
        gm.OnLevelStart();
        for (int a = 0; a < maps[levelSelected].enemyGroups.Count; a++)
        {
            for (int b = 0; b < maps[levelSelected].enemyGroups[a].enemySpawnSystems.Count; b++)
            {
                maps[levelSelected].enemyGroups[a].enemySpawnSystems[b].currentEnemyObject = 0;
            }
        }
    }

    void Update()
    {
        //level selection
        if (/*Input.GetKeyDown(gm.resetKey)*/gm.resetSignal)
        {
            LevelCleaner();
        }
    }

    public void LevelCleaner()
    {
        ps.barrierEnergyTankCapacityMeter = ps.barrierEnergyTankOrginalCapacitySize;
        gm.ClearEnemyBulletLibrary();
        levelSelected++;
        if (levelSelected >= maps.Count)
        {
            levelSelected = 0;
        }
        gm.OnLevelStart();
        vm.ResetMinimapPos();
        for (int a = 0; a < maps[levelSelected].enemyGroups.Count; a++)
        {
            for (int b = 0; b < maps[levelSelected].enemyGroups[a].enemySpawnSystems.Count; b++)
            {
                maps[levelSelected].enemyGroups[a].enemySpawnSystems[b].currentEnemyObject = 0;
            }
        }
    }

    public int GetGroupNumber(GameObject gObject)
    {
        int a = 0;
        while (maps[levelSelected].enemyGroups.Count > a && gObject.name != null && gObject.name != maps[levelSelected].enemyGroups[a].spawnerObject.name)
        {
            a++;
            if (a >= 50 || maps[levelSelected].enemyGroups[a] == null)
            {
                Debug.LogError("GetGroupNumber, did not find any results.");
                return (0);
            }
        }
        if (maps[levelSelected].enemyGroups[a] == null)
        {
            Debug.LogError("GetGroupNumber out of range." + gObject);
        }
        return (a);
    }

    //Register enemySpawners using BackgroundManager
    public void EnemyObjectLibrary()
    {
        //Clean up previous spawners
        if (enemyTransforms.Count != 0)
        {
            enemyTransforms.Clear();
        }

        //Add enemySpawner Transforms to list from under background manager (bm)
        foreach (Transform ob in bm.enemyObjects.transform)
        {
            enemyTransforms.Add(ob);
        }

        //Add enemySpawner Transforms object to: enemyGroup, List class
        int groupNumber = 0;
        while (maps[levelSelected].enemyGroups.Count > groupNumber && enemyTransforms.Count > groupNumber)
        {
            maps[levelSelected].enemyGroups[groupNumber].spawnerObject = enemyTransforms[groupNumber].gameObject;
            groupNumber++;
        }
    }

    //receive enemyObject name and save it to correct group
    public void SendEnemyData(GameObject senderObj, GameObject enemyObj)
    {
        int groupNumber = 0;
        //Find spawner
        while (groupNumber < maps[levelSelected].enemyGroups.Count && senderObj != maps[levelSelected].enemyGroups[groupNumber].spawnerObject)
        {
            groupNumber++;
        }
        if (senderObj != maps[levelSelected].enemyGroups[groupNumber].spawnerObject)
        {
            print("Spawner slot found");
        }
        int slotNumber = 0;
        //Find free spot
        if (maps[levelSelected].enemyGroups[groupNumber] != null)
        {
            while (maps[levelSelected].enemyGroups[groupNumber].enemySpawnSystems.Count > slotNumber && maps[levelSelected].enemyGroups[groupNumber].enemySpawnSystems[slotNumber].currentEnemyObject != 0)
            {
                slotNumber++;
            }
            if (maps[levelSelected].enemyGroups[groupNumber].enemySpawnSystems != null && maps[levelSelected].enemyGroups[groupNumber].enemySpawnSystems.Count > slotNumber)
            {
                //Add EnemyObject under spawner group to a free slot
                if (maps[levelSelected].enemyGroups[groupNumber].enemySpawnSystems[slotNumber].currentEnemyObject == 0)
                {
                    //print(enemyGroups[groupNumber].folderName + ", was filled with: " + enemyObj.GetInstanceID());
                    maps[levelSelected].enemyGroups[groupNumber].enemySpawnSystems[slotNumber].currentEnemyObject = enemyObj.GetInstanceID();
                }
            }
            else
            {
                Debug.LogError("Ei tarpeeksi tilaa ryhm�n slotissa tai slotit ovat t�ynn�. (tai t�m� funktio kutsutaan liian usein)");
            }
        }
    }

    public int GetEnemyListGroupNumber(GameObject gObject)
    {
        int a = 0;
        
        while (maps[levelSelected].enemyGroups.Count > a)
        {
            int b = 0;
            while (maps[levelSelected].enemyGroups[a].enemySpawnSystems.Count > b)
            {
                if (maps[levelSelected].enemyGroups[a].enemySpawnSystems[b].currentEnemyObject != 0 && maps[levelSelected].enemyGroups[a].enemySpawnSystems[b].currentEnemyObject == gObject.GetInstanceID())
                {
                    return (a);
                }
                b++;
            }
            a++;
        }
        return (999);
    }
    public int GetEnemyListSlotNumber(GameObject gObject)
    {
        int a = 0;
        int b = 0;
        while (maps[levelSelected].enemyGroups.Count > a)
        {
            b = 0;
            while (maps[levelSelected].enemyGroups[a].enemySpawnSystems.Count > b)
            {
                if (maps[levelSelected].enemyGroups[a].enemySpawnSystems[b].currentEnemyObject == gObject.GetInstanceID())
                {
                    return (b);
                }
                b++;
            }
            a++;
        }
        return (999);
    }
}
