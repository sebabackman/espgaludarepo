using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBase : MonoBehaviour
{
    RaycastHit2D hit;
    RaycastHit2D[] coll;
    public GameObject pLaserObject;
    public float laserLenghtOffset = 2f;
    public float laserPositionOffset = 2f;

    [Header("Don't touch!")]
    public float damage = 0;
    PlayerShooting ps;
    PlayerMovement pm;
    GameManager gm;
    int module = 0;
    int type = 0;
    int powerLevel = 0;
    Rigidbody2D rb;
    float distance = 0f;
    float maxDistance = 0f;
    float spacing = 0f;
    float width = 0f;
    float lenght = 0f;
    float rotate = 0f;
    bool isLaser = false;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].lifetimeOnOff)
        {
            Destroy(this.gameObject);
        }
    }

    void Awake()
    {
        ps = FindObjectOfType<PlayerShooting>();
        pm = FindObjectOfType<PlayerMovement>();
        gm = FindObjectOfType<GameManager>();
        spacing = ps.totalSpacing;
        module = ps.modules;
        type = ps.currentWeapon;
        powerLevel = ps.powerUpLevel;
        width = ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].projectileWidth;
        lenght = ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].projectileLenght;
        damage = ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].damage;
        maxDistance = ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].laserMaxDistance;
        rotate = ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].rotate;
        isLaser = ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].isLaser;
        if (isLaser)
        {
            if (distance > maxDistance || distance <= 0 || hit.collider == null)
            {
                distance = maxDistance;
            }
            pLaserObject.transform.position = new Vector3(transform.position.x, transform.position.y + 1 / laserPositionOffset);
            pLaserObject.transform.localScale = new Vector3(ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].laserWidht
            ,1 / laserLenghtOffset);
        }
    }

    void Start()
    {
        if (ps.barrierAttackInProgress)
        {
            damage *= ps.barrierAttackAddMoreDamageMultiplier * 10;
        }
        if (ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].lifetimeOnOff)
        {
            ps.DestroyBullet(this.gameObject);
        }
        rb = GetComponent<Rigidbody2D>();
        if (ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].isBullet)
        {
            Destroy(this.gameObject, 10);
        }
        if (ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].isBullet)
        {
            transform.localScale = new Vector3(width, lenght, 0f);
            rb.velocity = transform.up * ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].projectileSpeed * Time.deltaTime * 80;
        }
    }

    //Match all bullets with players x axis
    void StiffBullet()
    {
        if (Input.GetAxisRaw("Horizontal") != 0 && (transform.position.x - pm.transform.position.x) != 0 && !pm.hitLeftRight )
        {
            rb.velocity = new Vector3(Input.GetAxisRaw("Horizontal") * pm.playerSpeed * pm.slowDownMultiplier, 1f * ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].projectileSpeed, 0f) 
                 * Time.deltaTime * 100;
        }
        else
        {
            rb.velocity = transform.up * ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].projectileSpeed * Time.deltaTime * 80;
        }
    }
    
    RaycastHit2D FilterColliders()
    {
        coll = Physics2D.RaycastAll(transform.position, Vector2.up, maxDistance);
        for (int a = 0; a < coll.Length; a++)
        {
            if (coll != null && coll[a].collider != null)
            {
                if (coll[a].collider.CompareTag("EnemyObject"))
                {
                    return (coll[a]);
                }
                else if (coll[a].collider.name == "WallTop")
                {
                    return (coll[a]);
                }
                
            }
        }
        return (coll[0]);
    }

    //Laser movement, currently does not support rotation
    void LaserMovement()
    {
        
        hit = FilterColliders();
        if (hit.collider != null)
        {
            distance = Mathf.Abs(hit.point.y - transform.position.y);                     
        }
        if (distance > maxDistance || distance <= 0 || hit.collider == null)
        {
            distance = maxDistance;
            print("here");
        }
        transform.position = new Vector3(spacing + ps.transform.position.x, ps.transform.position.y);
        pLaserObject.transform.Rotate(0f, rotate, 0f);
        pLaserObject.transform.position = new Vector3(transform.position.x, transform.position.y + distance / laserPositionOffset);
        pLaserObject.transform.localScale = new Vector3(ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].laserWidht
            ,(1f + distance) / laserLenghtOffset);
        
    }

    void Update()
    {
        if (isLaser)
        {
            LaserMovement();
        }
    }

    private void FixedUpdate()
    {
        if (isLaser)
        {
            if (ps.type != type || pm.stunTimer > 0)
            {
                Destroy(this.gameObject);
            }
        }
        
        if (ps.weaponType[type].weaponPowerLevel[powerLevel].weaponAmmo[module].isStiff)
        {
            StiffBullet();
        }
    }
}
