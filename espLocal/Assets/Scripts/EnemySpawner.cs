using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    LevelManager lm;
    GameManager gm;
    public int currentLevel = 0;

    bool oneTimeActivation = true;

    public int groupCount = 0;
    public int slotCounter = 0;

    public float spawnTimer = 1f;

    int failsafe = 0;
    float spawnDelay = 0;

    bool startSpawner = false;
    bool done = false;
    Collision2D coll;
    Vector3 spawnPosition = new Vector3(0f, 0f, 0f);

    private void Start()
    {
        lm = FindObjectOfType<LevelManager>();
        gm = FindObjectOfType<GameManager>();
        SpriteRenderer spr;
        spr = GetComponent<SpriteRenderer>();
        spr.enabled = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "SpawnArea")
        {
            spawnPosition = this.transform.position;
            startSpawner = true;
            coll = collision;
        }
    }

    //Spawn and register enemies
    void SpawnManager()
    {
        if (/*Input.GetKeyDown(gm.resetKey)*/gm.resetSignal)
        {
            done = true;
        }
        failsafe = 0;
        if (!done)
        {
            currentLevel = lm.levelSelected;
            failsafe++;
            if (spawnDelay <= 0)
            {
                if (coll != null && lm.maps[lm.levelSelected].enemyGroups[groupCount].enemySpawnSystems[slotCounter].enemyObject != null)
                {
                    GameObject enemy = Instantiate(lm.maps[lm.levelSelected].enemyGroups[groupCount].enemySpawnSystems[slotCounter].enemyObject, spawnPosition, transform.rotation);
                    enemy.transform.parent = this.transform;
                    if (enemy != null && gameObject != null && lm != null)
                    {
                        lm.SendEnemyData(this.gameObject, enemy);
                    }
                }
            }
            //safegurad against infinite loop
            if (failsafe >= 100)
            {
                done = true;
                Debug.LogError("InfiniteLoopError");
            }
            //print(gameObject.name + " ,level selected: " + lm.levelSelected + " ,groupCount: " + groupCount);
            if (lm.maps[lm.levelSelected].enemyGroups[groupCount].enemySpawnSystems != null && slotCounter < lm.maps[lm.levelSelected].enemyGroups[groupCount].enemySpawnSystems.Count && spawnDelay <= 0)
            {
                slotCounter++;
                spawnDelay = lm.maps[lm.levelSelected].enemyGroups[groupCount].spawnDelay;
            }
            if (slotCounter >= lm.maps[lm.levelSelected].enemyGroups[groupCount].enemySpawnSystems.Count || currentLevel != lm.levelSelected)
            {
                done = true;
            }
        }
    }

    void Update()
    {
        if (oneTimeActivation)
        {
            oneTimeActivation = false;
            groupCount = lm.GetGroupNumber(this.gameObject);
            spawnDelay = lm.maps[lm.levelSelected].enemyGroups[groupCount].spawnDelay;
        }
        if (startSpawner && !done)
        {
            spawnDelay -= Time.deltaTime;
            SpawnManager();
        }
    }
}
