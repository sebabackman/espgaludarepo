using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[System.Serializable]
public class MenuManager : MonoBehaviour
{
    BackgroundManager bm;
    GameManager gm;
    ScoreManager sm;
    LevelManager lm;
    PlayerMovement pm;

    PlayerControlls control;

    public GameObject buttons;
    public GameObject textAreas;
    public GameObject menuBackground;

    void Start()
    {
        
        bm = FindObjectOfType<BackgroundManager>();
        gm = FindObjectOfType<GameManager>();
        sm = FindObjectOfType<ScoreManager>();
        lm = FindObjectOfType<LevelManager>();
        pm = FindObjectOfType<PlayerMovement>();
    }

    
    public void StartTheGame()
    {
        sm.currentScore = sm.preScore = 0;
        gm.playerScore.text = sm.currentScore.ToString();
        gm.gemCounter = 0;
        gm.gemCountText.text = gm.gemCounter.ToString();
        gm.goldCounter = 0;
        gm.goldCountText.text = gm.goldCounter.ToString();
        lm.LevelCleaner();
        lm.levelSelected = 1;
        pm.transform.position = new Vector3(0f, 0f, 0f);
        gm.levelOnOff = true;
        buttons.gameObject.SetActive(false);
        menuBackground.gameObject.SetActive(false);
        textAreas.gameObject.SetActive(true);
    }

    public void EndTheGame()
    {
        print(":(");
        Application.Quit();
    }

    public void BackToMainMenu()
    {
        lm.levelSelected = 0;
        gm.lifeCounter = gm.maxLifes;
        Time.timeScale = 1;
        gm.OnLevelStart();
        gm.continueText.text = "";
        gm.continueInput = true;
        bm.EnemyLayoutController();
        sm.preScore = sm.currentScore = 0;
        gm.levelOnOff = false;
        buttons.gameObject.SetActive(true);
        menuBackground.gameObject.SetActive(true);
        textAreas.gameObject.SetActive(false);
    }

    void Update()
    {
        

    }
}
