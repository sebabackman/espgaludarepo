using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {
    bool andAction = false;
    PlayerShooting ps;
    ProjectileBase pb;
    LevelManager lm;
    GameManager gm;
    ScoreManager sm;

    
    public int gemDrain = 0;
    GameObject lifeObject;
    GameObject goldObject;
    GameObject powerUpObject;
    GameObject energyObject;
    GameObject maxPowerObject;

    //data
    int group = 0;
    int slot = 0;

    //basic
    public GameObject gemPrefab = null;
    public float health = 100;
    bool dropExtraLife = false;
    bool dropPowerup = false;
    bool dropMaxPowerup = false;
    bool dropEnergy = false;

    //HomingMovement
    bool isHoming = false;
    float homingSpeed = 1f;

    //waypoints
    bool isWaypoint = false;
    List<Transform> destinations = new List<Transform>();

    //direct
    bool isDirect = false;
    Vector3 directSpeed = new Vector2(1f, 1f);
    bool directStop = false;
    bool directChangeDirection = false;
    Vector3 directNewDirection = new Vector3(1f, 1f);
    float directTimer = 0f;

    //wavy
    bool isWavy = false;
    bool horizontal = false;
    float wavySpeed = 1f;
    float time = 1f;
    float waveFrequency = 1f;

    public float slowSpeed = 1f;
    float currentVelocity = 1f;

    float reverser = 1f;
    float timer = 0f;
    public bool noDrops = false;

    public int amountOfGems = 1;
    public int constantPoints = 10;
    public int pointsOnDestruction = 500;
    bool destroyFailSafe = false;

    int currentLevel = 0;

    //Tiket number to identify right enemyObjects

    private void Awake() 
    {
        lm = FindObjectOfType<LevelManager>();
        gm = FindObjectOfType<GameManager>();
        ps = FindObjectOfType<PlayerShooting>();
        sm = FindObjectOfType<ScoreManager>();
    }

    void Start() 
    {
        SendCriticalData();
        lifeObject = gm.lifeObject;
        goldObject = gm.goldObject;
        powerUpObject = gm.powerUpObject;
        energyObject = gm.energyObject;
        maxPowerObject = gm.maxPowerObject;
    }

    private void OnCollisionEnter2D(Collision2D collision) 
    {
        //take damage
        if (collision.gameObject != null)
        {
            pb = collision.transform.GetComponent<ProjectileBase>();
            if (pb == null) 
            {
                pb = collision.transform.GetComponentInParent<ProjectileBase>();
            }
            if (pb != null && andAction) 
            {
                health -= pb.damage;
                if (!gm.kakuseiModeSwitch) {
                    sm.AddScore(constantPoints);
                }
                if (ps.barrierAttackDurationTimer > 0)
                {
                    noDrops = true;
                }
            }
            if (collision.gameObject.name == "ActiveArea") 
            {
                andAction = true;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision) 
    {
        //take constant damage
        if (collision.gameObject != null) 
        {
            pb = collision.transform.GetComponent<ProjectileBase>();
            if (pb == null) 
            {
                pb = collision.transform.GetComponentInParent<ProjectileBase>();
            }
            if (pb != null && andAction) 
            {
                health -= pb.damage;
                if (!gm.kakuseiModeSwitch) {
                    sm.AddScore(constantPoints);
                }
                if (ps.barrierAttackDurationTimer > 0)
                {
                    noDrops = true;
                }
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject != null && !destroyFailSafe)
        {
            if (collision.gameObject.name == "ActiveArea")
            {
                gm.EnemyProjectileObjectRemove(this.gameObject, true);
                Destroy(this.gameObject);
            }
        }
    }

    float KakuseiModifier()
    {
        float speed = 1;
        if (gm.kakuseiModeSwitch && gm.gemCounter != 0)
        {
            speed = slowSpeed;
        }
        return (speed);
    }

    void WaypointMovement()
    {

    }

    void HomingMovement() {

    }

    void DirectMovement() {
        //if (directStop && directTimer == 0)
        //{
        //    directTimer = directCooldown;
        //}
        //if (directStop && !directOneTime)
        //{
        //    directTimer -= Time.deltaTime;
        //}
        if (directTimer > 0 || !directStop)
        {
            transform.position += directSpeed * Time.deltaTime / KakuseiModifier();
        }
        if (directChangeDirection)
        {
            transform.position += directNewDirection * Time.deltaTime / KakuseiModifier();
        }

    }

    void WavyMovement() {
        if (/*currentVelocity >= waveSize*/ timer <= 0) {
            timer = time;
            reverser *= -1f;
        }
        currentVelocity = reverser * waveFrequency;
        if (horizontal)
        {
            transform.position += new Vector3(wavySpeed, currentVelocity, 0f) * Time.deltaTime / KakuseiModifier();
        }
        else
        {
            transform.position += new Vector3(currentVelocity, wavySpeed, 0f) * Time.deltaTime / KakuseiModifier();
        }
    }

    public void SendCriticalData() {
        //Data
        group = lm.GetEnemyListGroupNumber(this.gameObject);
        slot = lm.GetEnemyListSlotNumber(this.gameObject);
        if (slot >= 999 || group >= 999) {
            group = 0;
            slot = 0;
            Debug.LogError(this.gameObject.name + ": Group or Slot Not Found");
        }

        //basic
        currentLevel = lm.levelSelected;
        //health = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].health;
        dropExtraLife = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].dropExtraLife;
        dropPowerup = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].dropPowerup;
        dropMaxPowerup = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].dropMaxPowerup;
        dropEnergy = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].dropEnergy;

        //waypoints
        isWaypoint = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].isWaypoint;
        destinations = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].destinations;
        //waypointSpeed = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].speed;

        //direct
        isDirect = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].isDirect;
        directSpeed = new Vector3(lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].dXSpeed, lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].dYSpeed);

        //wavy
        isWavy = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].isWavy;
        horizontal = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].horizontal;
        wavySpeed = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].wavySpeed;
        directTimer = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].waveSize;
        waveFrequency = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].waveFrequency;
        //waveDirection = lm.maps[lm.levelSelected].enemyGroups[group].enemySpawnSystems[slot].waveDirection;
    }

    void FixedUpdate()
    {
        timer -= Time.deltaTime / KakuseiModifier();
        if (health <= 0 && !destroyFailSafe)
        {
            destroyFailSafe = true;
            if (!noDrops)
            {
                if (gemPrefab != null && !gm.kakuseiModeSwitch && gm.gemCounter != 500)
                {
                    for (int a = 0; a < amountOfGems + gm.kakuseiOverModeExtraTotalGemDrop; a++)
                    {
                        var gem = Instantiate(gemPrefab);
                        gem.transform.position = transform.position + new Vector3(Random.Range(-2.0f, 2.0f), Random.Range(-2.0f, 2.0f));
                    }
                }
                if (gm.gemCounter == 500)
                {
                    Instantiate(goldObject, transform.position, transform.rotation);
                }
                if (gm.kakuseiModeSwitch)
                {
                    if (gm.gemCounter == 0)
                    {
                        Instantiate(goldObject, transform.position, transform.rotation);
                    }
                    if (!noDrops)
                    {
                        gm.EnemyProjectileObjectRemove(this.gameObject, false);
                    }
                    else
                    {
                        gm.EnemyProjectileObjectRemove(this.gameObject, true);
                    }
                    gm.gemCounter -= gemDrain;
                    if (gm.gemCounter < 0)
                    {
                        gm.gemCounter = 0;
                    }
                }
                if (lifeObject != null && dropExtraLife)
                {
                    Instantiate(lifeObject, transform.position, transform.rotation);
                }
                if (powerUpObject != null && dropPowerup)
                {
                    Instantiate(powerUpObject, transform.position, transform.rotation);
                }
                if (energyObject != null && dropEnergy)
                {
                    Instantiate(energyObject, transform.position, transform.rotation);
                }
            }
            sm.AddScore(pointsOnDestruction);
            Destroy(this.gameObject);
        }
        if (isDirect) {
            DirectMovement();
        }
        if (isWaypoint) {
            WaypointMovement();
        }
        if (isWavy) {
            WavyMovement();
        }
        if (isHoming) {
            HomingMovement();
        }
        if (lm.levelSelected != currentLevel)
        {
            Destroy(this.gameObject);
        }
    }
}
