using UnityEngine;

public class VisualManager : MonoBehaviour
{
    GameManager gm;
    GameObject player;
    GameObject miniMapObject;
    GameObject transparentObject;
    BackgroundManager bm;
    SpriteRenderer spr;
    LevelManager lm;
    public float sideMovement = 0.1f;
    [Tooltip("Used to reset Transparent object in x axis(needs a reference object).")]
    public Transform resetX;

    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        lm = FindObjectOfType<LevelManager>();
        player = GameObject.Find("PlayerContainer");
        bm = FindObjectOfType<BackgroundManager>();
        miniMapObject = GameObject.Find("MinimapManager");
        transparentObject = GameObject.Find("Transparent");
    }

    float KakuseiModifier()
    {
        float speed = 1;
        if (gm.kakuseiModeSwitch && gm.gemCounter != 0)
        {
            speed = lm.maps[lm.levelSelected].kakuseiModeSlow;
        }
        return (speed);
    }

    //Player camera perspective change
    void CameraPerspective()
    {
        this.transform.position = new Vector2(player.transform.position.x * sideMovement, 0f);
    }

    //Minimap
    void Minimap()
    {
        spr = miniMapObject.GetComponent<SpriteRenderer>();
        miniMapObject.transform.localScale = new Vector2(lm.maps[lm.levelSelected].minimapScaling, lm.maps[lm.levelSelected].minimapScaling);
        transparentObject.transform.position += new Vector3(0f, lm.maps[lm.levelSelected].minimapTransparentSpeed * Time.deltaTime / 7.1f, 0f) / KakuseiModifier();
        spr.sprite = bm.sprMap;
    }

    public void ResetMinimapPos()
    {
        resetX.position = new Vector2(resetX.position.x, lm.maps[lm.levelSelected].minimapHiderPos);
        resetX.localScale = lm.maps[lm.levelSelected].minimapHiderScale;
        transparentObject.transform.localScale = lm.maps[lm.levelSelected].minimapTransparentScale;
        transparentObject.transform.position = new Vector3(resetX.position.x , lm.maps[lm.levelSelected].minimapTransparentYStartPos, 0f);
    }

    void Update()
    {
        if (gm.levelOnOff)
        {
            CameraPerspective();
            Minimap();
        }
    }
}
