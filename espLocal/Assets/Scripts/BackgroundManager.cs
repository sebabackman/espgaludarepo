using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    GameManager gm;

    [Header("BossFightLoop")]
    public float bossFightRelocation = 0f;
    public Transform bossBackgroundEnd;

    [Tooltip("Put an object inside that is going to move Boss Fight Sprites.")]
    public List<GameObject> bossFightBackgroundsMovement = new List<GameObject>();

    [Tooltip("Put an object inside that has a SpriteRenderer.")]
    public List<SpriteRenderer> bossFightSprites = new List<SpriteRenderer>();

    public List<GameObject> bossFightObjects = new List<GameObject>();

    public Sprite sprMap;
    public Sprite sprBoss1;
    public Sprite sprBoss2;

    GameObject backgroundObj;
    GameObject enemyPosition;
    
    SpriteRenderer rend;
    LevelManager lm;

    [Header("Don't touch!")]
    public float bossFightStartPos = 109.15f;
    public float nextBossFightPos = 20f;
    float bossFightPos = 0f;
    public Vector2 backgroundScaling = new Vector2(1f, 1f);

    public GameObject enemyObjects;
    bool printed = false;
    public int currentLevel = 0;

    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        lm = FindObjectOfType<LevelManager>();
        this.transform.position = new Vector2(0f, lm.maps[lm.levelSelected].backgroundStartPos);
    }

    float KakuseiModifier()
    {
        float speed = 1;
        if (gm.kakuseiModeSwitch && gm.gemCounter != 0)
        {
            speed = lm.maps[lm.levelSelected].kakuseiModeSlow;
        }
        return (speed);
    }

    void StartBossFight()
    {
        foreach (GameObject a in bossFightBackgroundsMovement)
        {
            if (a.transform.position.y < bossBackgroundEnd.position.y)
            {
                
            }
        }
    }

    void BossFightBackgrounds()
    {
        //keep bossFightBackgrounds in a loop
        foreach (GameObject a in bossFightBackgroundsMovement)
        {
            if (a.transform.position.y < bossBackgroundEnd.position.y)
            {
                a.transform.position += new Vector3(0f, bossFightRelocation, 0f) / KakuseiModifier();
            }
        }
    }

    void BackgroundController()
    {
        //Fetch stuff
        sprMap = lm.maps[lm.levelSelected].mapSprite;
        sprBoss1 = lm.maps[lm.levelSelected].bossMap1;
        sprBoss2 = lm.maps[lm.levelSelected].bossMap2;

        enemyPosition = GameObject.Find("EnemyHolder");
        backgroundObj = GameObject.Find("Background");
        rend = backgroundObj.GetComponent<SpriteRenderer>();

        //Set background for the boss fight
        bossFightSprites[0].sprite = sprBoss1;
        bossFightSprites[1].sprite = sprBoss2;
        rend.sprite = sprMap;
    }

    public void EnemyLayoutController()
    {
        //Spawn enemy objects
        if (!printed)
        {
            backgroundObj.transform.localScale = new Vector2(lm.maps[lm.levelSelected].backgroundScaling, lm.maps[lm.levelSelected].backgroundScaling);
            bossFightStartPos = lm.maps[lm.levelSelected].bossFightStartPos;
            nextBossFightPos = lm.maps[lm.levelSelected].nextBossFightPos;
            enemyObjects = Instantiate(lm.maps[lm.levelSelected].enemyLayoutPrefab);
            enemyObjects.transform.SetParent(enemyPosition.transform);
            lm.EnemyObjectLibrary();
            printed = true;
            foreach (GameObject a in bossFightObjects)
            {
                a.transform.localScale = new Vector2(lm.maps[lm.levelSelected].bossFightBackgroundScaling, lm.maps[lm.levelSelected].bossFightBackgroundScaling);
            }
        }

        //Despawn enemy objects and reset background positions
        else if (printed && currentLevel != lm.levelSelected)
        {
            bossFightStartPos = lm.maps[lm.levelSelected].bossFightStartPos;
            nextBossFightPos = lm.maps[lm.levelSelected].nextBossFightPos;
            Destroy(enemyObjects);
            this.transform.position = new Vector2(0f, lm.maps[lm.levelSelected].backgroundStartPos);
            foreach (GameObject a in bossFightBackgroundsMovement)
            {
                a.transform.position = new Vector2(0f, bossFightStartPos + bossFightPos);
                bossFightPos += nextBossFightPos;
            }
            bossFightPos = 0;
            currentLevel = lm.levelSelected;
            printed = false;
        }
    }

    void Update()
    {
        if (gm.levelOnOff)
        {
            this.transform.position = this.transform.position + new Vector3(0f, -1f, 0f) * Time.deltaTime * lm.maps[lm.levelSelected].backgroundSpeed / KakuseiModifier();
            BossFightBackgrounds();
            BackgroundController();
            EnemyLayoutController();
        }
    }
}
