using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject powerUpPrefab;
    public float playerSpeed = 1;
    [Tooltip("Stop player from moving when taken damage.")]
    public float stunDuration = 1f;
    public float stunTimer = 0;
    public float damageImmunity = 1f;
    public float immunityTimer = 0;

    public float leftDir = 0;
    public float rightDir = 0;
    public float slowDownMultiplier = 1;
    float horizontal = 0;
    Rigidbody2D rb;
    PlayerShooting ps;
    GameManager gm;

    float movementCounterY = 0f;
    float movementCounterX = 0f;

    public GameObject wallLeft;
    public GameObject wallRight;
    public bool hitLeftRight = false;
    public GameObject wallTop;
    public GameObject wallBottom;

    private void OnTriggerEnter2D(Collider2D collision)
{
        if (collision.CompareTag("EnemyProjectile") && immunityTimer <= 0 && !ps.barrierAttackInProgress && ps.barrierAttackDurationTimer <= 0)
        {
            gm.kakuseiOverModeTimer = 0;
            gm.kakuseiOverModeExtraTotalBulletSpeed = 0;
            gm.kakuseiOverModeExtraTotalGemDrop = 0;
            if (gm.kakuseiModeSwitch && ps.barrierEnergyTankCapacityMeter >= ps.barrierEnergyTankOrginalCapacitySize / 2)
            {
                ps.barrierEnergyTankCapacityMeter -= ps.barrierEnergyTankOrginalCapacitySize / 2;
                print("took a hit in kakusei");
                ps.BarrierMode();
            }
            else
            {
                
                for (int a = 0; a < ps.powerUpLevel; a++)
                {
                    Instantiate(powerUpPrefab, transform.position + new Vector3(Random.Range(2.0f, 5.0f), Random.Range(-4.0f, 4.0f)), transform.rotation);
                }
                ps.powerUpLevel = 0;
                //ps.savePowerUpLevel = ps.powerUpLevel;
                ps.barrierEnergyTankCapacityMeter = ps.barrierEnergyTankOrginalCapacitySize;
                Stunned();
                gm.RemoveLife();
                gm.RemoveGold(false);
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        //Stop moving when against a obstacle and a bool check for ProjectileBase
        if (collision.gameObject == wallLeft || collision.gameObject == wallRight)
        {
            hitLeftRight = true;
        }
        else
        {
            hitLeftRight = false;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        //Stop moving when against a obstacle
        if (collision.gameObject == wallLeft || collision.gameObject == wallRight)
        {
            hitLeftRight = false;
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        ps = FindObjectOfType<PlayerShooting>();
        gm = FindObjectOfType<GameManager>();
    }

    void Stunned()
    {
        immunityTimer = damageImmunity;
        stunTimer = stunDuration;
    }

    public void ShotMovingDirection()
    {
        leftDir = ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].leftDir;
        rightDir = ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].rightDir;

        //dynamic shooting angle
        horizontal = Input.GetAxisRaw("Horizontal");
        if (ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].wideningShotOnOff)
        {
            if (horizontal < 0 && leftDir < 1f && /*Input.GetKey(gm.primaryWeaponKey)*/gm.primaryWeaponSignal || horizontal < 0 && leftDir < 1f && /*Input.GetKey(gm.secondaryWeaponKey)*/gm.secondaryWeaponSignal)
            {
                leftDir += ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].wideningSpeed;
            }
            else if (/*!Input.GetKey(gm.primaryWeaponKey)*/!gm.primaryWeaponSignal && leftDir > 0 || /*!Input.GetKey(gm.secondaryWeaponKey)*/!gm.secondaryWeaponSignal && leftDir > 0)
            {
                leftDir -= ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].recoverySpeed;
            }
            if (horizontal > 0 && rightDir < 1f && /*Input.GetKey(gm.primaryWeaponKey)*/gm.primaryWeaponSignal || horizontal > 0 && rightDir < 1f && /*Input.GetKey(gm.secondaryWeaponKey)*/gm.secondaryWeaponSignal)
            {
                rightDir += ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].wideningSpeed;
            }
            else if (/*!Input.GetKey(gm.primaryWeaponKey)*/!gm.primaryWeaponSignal && rightDir > 0 || /*!Input.GetKey(gm.secondaryWeaponKey)*/!gm.secondaryWeaponSignal && rightDir > 0)
            {
                rightDir -= ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].recoverySpeed;
            }
            ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].leftDir = leftDir;
            ps.weaponType[ps.currentWeapon].weaponPowerLevel[ps.powerUpLevel].weaponAmmo[ps.modules].rightDir = rightDir;
        }
    }

    public void KakuseiOvermodeShadow()
    {
        
    }

    private void FixedUpdate()
    {
        if (gm.levelOnOff)
        {
            if (immunityTimer > 0)
            {
                immunityTimer -= Time.fixedDeltaTime;
                stunTimer -= Time.fixedDeltaTime;
            }

            //Player movement
            if (Input.GetAxisRaw("Horizontal") != 0 && stunTimer <= 0 || Input.GetAxisRaw("Vertical") != 0 && stunTimer <= 0)
            {
                rb.velocity = new Vector3(Input.GetAxisRaw("Horizontal") - movementCounterX, Input.GetAxisRaw("Vertical") - movementCounterY) * playerSpeed * slowDownMultiplier * Time.fixedDeltaTime * 100;
            }
            else
            {
                rb.velocity = new Vector3(0f, 0f, 0f);
            }
        }
    }
}
