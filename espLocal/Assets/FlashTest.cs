using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashTest : MonoBehaviour
{
    SpriteRenderer spr;
    int flashId;
    // Start is called before the first frame update
    void Awake()
    {
        spr = GetComponent<SpriteRenderer>();
        //var shader = spr.material.shader;
        //flashId = shader.FindPropertyIndex("");
    }

    // Update is called once per frame
    void Update()
    {
        spr.material.SetFloat("_FlashAmount", Input.GetKey(KeyCode.V) ? 1 : 0 );
    }
}
